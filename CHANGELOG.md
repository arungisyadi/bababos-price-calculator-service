# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.2 (2023-10-20)


### Features

* initiate the main logic to provide price estimation. ([b21445f](https://gitlab.com/arungisyadi/bababos-price-calculator-service/commit/b21445f14cf50040515fcfcc61a942ea8814e383))
* setup the views and routes. ([383ab07](https://gitlab.com/arungisyadi/bababos-price-calculator-service/commit/383ab0754370113258a9477ae5cb08fa0ede2aca))

### 0.0.1 (2023-10-20)


### Features

* initiate the main logic to provide price estimation. ([b21445f](https://gitlab.com/arungisyadi/bababos-price-calculator-service/commit/b21445f14cf50040515fcfcc61a942ea8814e383))
* setup the views and routes. ([383ab07](https://gitlab.com/arungisyadi/bababos-price-calculator-service/commit/383ab0754370113258a9477ae5cb08fa0ede2aca))
