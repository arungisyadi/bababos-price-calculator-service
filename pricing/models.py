from django.db import models

# Create your models here.
class HistoricalSalesData(models.Model):
    # Define fields for historical sales data
    date = models.DateField()
    product_name = models.CharField(max_length=255)
    sale_price = models.DecimalField(max_digits=10, decimal_places=2)

class CompetitorData(models.Model):
    # Define fields for competitor data
    competitor_name = models.CharField(max_length=255)
    # Add other relevant fields

class CustomerData(models.Model):
    # Define fields for customer data
    customer_name = models.CharField(max_length=255)