from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint
from .pricing_logic import PricingEngine

@csrf_exempt
def calculate_price_view(request):
    price = 0
    if request.method == 'POST':
        price = float(request.POST['price'])
        sku = request.POST['sku']
    
    # raise Exception(price)
    sales_data = (0.15 * price) + price
    competitor_data = (0.25 * price) + price
    customer_data = (0.1 * price) + price

    pricing_engine = PricingEngine(sales_data, competitor_data, customer_data)
    price_recommendation = pricing_engine.calculate_price_recommendation()

    return JsonResponse(
        {
            'item': sku,
            'price_recommendation': price_recommendation
        }
    )

def get_sales_data_from_request(request):
    return 0

def get_competitor_data_from_request(request):
    return 0

def get_customer_data_from_request(request):
    return 0